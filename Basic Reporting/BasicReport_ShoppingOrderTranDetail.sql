USE [MaestroReporting]
GO
/****** Object:  StoredProcedure [Report].[BasicReport_ShoppingOrderTranDetail]    Script Date: 3/2/2015 1:51:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [Report].[BasicReport_ShoppingOrderTranDetail] 
 @TenantID INT
,@UserID INT
,@ShipCountryCode varchar(1000)
,@ItemTitle varchar(1000)
,@ItemType varchar(1000)
,@IsVoid varchar(1000)
,@StartVoidDateTime varchar(1000)
,@EndVoidDateTime varchar(1000)
,@StartOrderDateTime varchar(1000)
,@EndOrderDateTime varchar(1000)


AS
BEGIN

DECLARE @QUERY varchar(max)
DECLARE @WHERE varchar(max)

SET @WHERE = ''

if (@ShipCountryCode <> '' and @ShipCountryCode is not null)
	SELECT @WHERE = @WHERE + ' AND usract.ShopperHistEffShipCountryCode = ''' + @ShipCountryCode + ''''

if(@ItemTitle  <> '' and @ItemTitle is not null)
	SELECT @WHERE = @WHERE + ' AND odrfct.ItemTitle = ''' + @ItemTitle  + ''''

if(@ItemType  <> '' and @ItemType is not null)
	SELECT @WHERE = @WHERE + ' AND odrfct.ItemType = ''' + @ItemType  + ''''

if(@IsVoid  <> '' and @IsVoid is not null)
	SELECT @WHERE = @WHERE + ' AND odrfct.Void = ''' + @IsVoid  + ''''

if((@StartVoidDateTime  <> '' and @EndVoidDateTime <> '') and (@StartVoidDateTime  is not null and @EndVoidDateTime is not null))
	SELECT @WHERE = @WHERE + ' AND odrfct.VoidDate between ''' + @StartVoidDateTime + ''' and ''' + @EndVoidDateTime + ''''

if((@StartOrderDateTime  <> '' and @EndOrderDateTime <> '') and (@StartOrderDateTime  is not null and @EndOrderDateTime is not null))
	SELECT @WHERE = @WHERE + ' AND odrfct.OrderDT between ''' + @StartOrderDateTime + ''' and ''' + @EndOrderDateTime + ''''




--SELECT @WHERE


SET @QUERY =

	'SELECT OrderID
		,ShopperCustAcctCode
		,ShopperFullName
		,ShopperHistEmailAddr
		,ShopperHistEffShipCountryCode
		,OrderDateTime
		,OrderDateTimeFiscYear
		,OrderDateTimeFiscQtr
		,OrderDateTimeFiscTrim
		,OrderDateTimeCalMth
		,ItemLineID
		,ItemID
		,ItemTitle
		,ItemType
		,TotalPoints
		,ShipVia
		,TrackingNumber
		,ShippingStatus
		,Void
		,VoidDateTime
		,ShopperCustomAttributeName
		,ShopperCustomAttributeValue
	FROM (
		SELECT odrfct.ShopOrderID AS OrderID
			,usract.ShopperCustAcctCode
			,usract.ShopperFullName
			,usract.ShopperAccessLevelCode
			,usract.ShopperEmpStatusCode
			,odrfct.OrderEmail AS ShopperHistEmailAddr
			,usract.ShopperHistEffShipCountryCode
			,odrfct.OrderDT AS OrderDateTime
			,odrfct.OrderFiscalYear AS OrderDateTimeFiscYear
			,odrfct.OrderFiscalQuarter AS OrderDateTimeFiscQtr
			,odrfct.OrderFiscalTrimester AS OrderDateTimeFiscTrim
			,odrfct.OrderCalendarMonth AS OrderDateTimeCalMth
			,odrfct.LineItemID AS ItemLineID
			,odrfct.ItemID
			,odrfct.ItemTitle
			,odrfct.ItemType
			--,ittloc.ItemDesc AS ItemTypeDescr
			,odrfct.ItemPoints AS TotalPoints
			,odrfct.ShipVia
			,odrfct.TrackingNumber
			,odrfct.ShippingStatus
			,odrfct.Void
			,odrfct.VoidDate AS VoidDateTime
			,usract.ShopperCustomAttributeName
			,usract.ShopperCustomAttributeValue
		FROM Report.Tenant t
		INNER JOIN Report.tbShopOrderItemFact AS odrfct ON t.TenantID = odrfct.TenantID
		--INNER JOIN [Report].[tbItemTypeLocale] AS ittloc
		--	ON ittloc.ItemType = odrfct.ItemType
		LEFT OUTER JOIN (
			SELECT ua.TenantID AS ShopperTenantID
				,ua.UserAccountKey AS ShopperUserAccountKey
				,ua.UserAccountID AS ShopperUserAccountID
				,ua.InternalUserCode AS ShopperCustAcctCode
				,ua.PersonFullName AS ShopperFullName
				,ua.UserAccessLevelCode AS ShopperAccessLevelCode
				,ua.[UserEmploymentStatus] AS ShopperEmpStatusCode
				,ua.ShipCountryCode AS ShopperHistEffShipCountryCode
				,ucv.[CustomAttributeName] AS ShopperCustomAttributeName
				,ucv.[CustomAttributeValue] AS ShopperCustomAttributeValue
			FROM [Report].[UserAccount] ua
			LEFT OUTER JOIN Report.UserCustomValue ucv ON ua.UserAccountKey = ucv.UserAccountKey AND ua.TenantID = ucv.TenantID'
				--SELECT TenantID as ShopperTenantID, UserAccountKey as ShopperUserAccountKey, UserAccountID as ShopperUserAccountID
				--	,InternalUserCode AS ShopperCustAcctCode
				--	,PersonFullName AS ShopperFullName
				--	,UserAccessLevelCode AS ShopperAccessLevelCode
				--	,[UserEmploymentStatus] AS ShopperEmpStatusCode
				--	,ShipCountryCode AS ShopperHistEffShipCountryCode
				--,ca.i.value('(@name)','nvarchar(50)') as ShopperCustomAttributeName
				--,ca.i.value('(@value)','nvarchar(150)') as ShopperCustomAttributeValue  
				--FROM [Report].[UserAccount] 
				--CROSS APPLY CustomAttributes.nodes('/ucv[@id=1 or @id=2 or @id=3 or @id=4 or @id=5 or @id=6 or @id=7 or @id=8 or @id=9 or @id=10 or @id=11 or @id=12 or @id=13 or @id=14 or @id=15]') AS ca(i) 
			+') usract ON (
				usract.ShopperUserAccountKey = odrfct.UserAccountKey
				AND usract.ShopperTenantID = t.TenantID
				)
		--LEFT JOIN Report.GetUserOrgTreeWithAncestor(@UserID,@TenantID)
		--	ON GetUserOrgTreeWithAncestor.UserAccountID = usract.ShopperUserAccountID
		WHERE t.TenantID = '  + Cast(@TenantID as varchar) 
						 + @WHERE + ' 
		) a
	ORDER BY 1 '


	Insert INTO [Report].[RDLQueryHistory] ([Timestamp], SQLQuery)
	Select GETDATE(), @Query

    Exec (@QUERY)

	--select @QUERY
	--select @WHERE

END
