USE [MaestroReporting]
GO
/****** Object:  StoredProcedure [Report].[BasicReport_ShoppingPointTranDetail]    Script Date: 3/9/2015 2:38:29 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [Report].[BasicReport_ShoppingPointTranDetail] 
@TenantID int,
@UserID int,
@ShipCountry varchar(1000),
@EmployeeStatusCode varchar(1000),
@AccessLevelCode varchar(1000),
@PointTransactionTypeDescription varchar(1000),
@RecognitionTypeName varchar(1000),
@StartTransactionDateTime varchar(1000),
@EndTransactionDateTime varchar(1000)

--@StartDate datetime,
--@EndDate datetime
AS
BEGIN

DECLARE @QUERY varchar(max)
DECLARE @WHERE varchar(max)

SET @WHERE = ''

if (@ShipCountry <> '' and @ShipCountry is not null)
	SELECT @WHERE = @WHERE + ' AND u.UserHistEffShipCountryCode = ''' + @ShipCountry + ''''

if(@EmployeeStatusCode  <> '' and @EmployeeStatusCode is not null)
	SELECT @WHERE = @WHERE + ' AND u.UserEmpStatusCode = ''' + @EmployeeStatusCode  + ''''

if(@AccessLevelCode  <> '' and @AccessLevelCode is not null)
	SELECT @WHERE = @WHERE + ' AND u.UserAccessLevelCode = ''' + @AccessLevelCode  + ''''

if(@PointTransactionTypeDescription  <> '' and @PointTransactionTypeDescription is not null)
	SELECT @WHERE = @WHERE + ' AND ptt.PointTranTypeDesc = ''' + @PointTransactionTypeDescription  + ''''

if(@RecognitionTypeName  <> '' and @RecognitionTypeName is not null and @RecognitionTypeName <> 'no_value_passed')
	SELECT @WHERE = @WHERE + ' AND r.RecognitionTypeCode = ''' + @RecognitionTypeName  + ''''

if((@StartTransactionDateTime  <> '' and @EndTransactionDateTime <> '') and (@StartTransactionDateTime  is not null and @EndTransactionDateTime is not null))
	SELECT @WHERE = @WHERE + ' AND f.ShopPointDT between ''' + @StartTransactionDateTime + ''' and ''' + @EndTransactionDateTime + ''''

SET @QUERY =

		'SELECT
		PointTranID, UserCustAcctCode, UserFullName, UserEmailAddr, UserHistEffShipCountryCode
		, UserEmpStatusCode, UserAccessLevelCode
		, PointTranType, PointTranTypeDesc, PointAmount
		, TranDateTime, TranFiscYear, TranFiscQtr, TranFiscTrim, TranCalMth
		, AwardID, AwdSourceType, RecogTypeCode, CurrRecogTypeName
		, ShopOrderID, UserCustomAttributeName, UserCustomAttributeValue
		FROM
		(SELECT
		f.PointTransID AS PointTranID
		,u.UserCustAcctCode
		,u.UserFullName
		,u.UserEmailAddr
		,u.UserHistEffShipCountryCode
		,u.UserEmpStatusCode
		,u.UserAccessLevelCode
		,f.PointTranType
		,ptt.PointTranTypeDesc
		,f.PointAmt AS PointAmount
		,f.ShopPointDT AS TranDateTime
		,f.ShopPointFiscalYear AS TranFiscYear
		,f.ShopPointFiscalQuarter AS TranFiscQtr
		,f.ShopPointFiscalTrimester AS TranFiscTrim
		,f.ShopPointCalendarMonth AS TranCalMth
		,f.UserRecognitionID AS AwardID
		,a.AwardSourceType AS AwdSourceType
		,r.RecognitionTypeCode AS RecogTypeCode
		,r.RecognitionTypeName AS CurrRecogTypeName
		,f.ShopPointOrderID AS ShopOrderID
		,u.UserCustomAttributeName
		,u.UserCustomAttributeValue
		,1 AS Lvl --	GetUserOrgTreeWithAncestor.Lvl
	FROM Report.Tenant t
		INNER JOIN Report.tbShopPointTransactionFact AS f
			ON t.TenantID = f.TenantID
		 LEFT OUTER  JOIN (
		SELECT TenantID, UserAccountKey, UserAccountID
			,[InternalUserCode] AS UserCustAcctCode
			,[PersonFullName] AS UserFullName
			,[EmailLogin] AS UserEmailAddr
			,[ShipCountryCode] AS UserHistEffShipCountryCode
			,UserEmploymentStatus AS UserEmpStatusCode
			,UserAccessLevelCode ,ca.i.value(''(@name)'',''nvarchar(50)'') as UserCustomAttributeName
						,ca.i.value(''(@value)'',''nvarchar(150)'') as UserCustomAttributeValue  FROM [Report].[UserAccount] CROSS APPLY CustomAttributes.nodes(''/ucv[@id=1 or @id=2 or @id=3 or @id=4 or @id=5 or @id=6 or @id=7 or @id=8 or @id=9 or @id=10 or @id=11 or @id=12 or @id=13 or @id=14 or @id=15]'') AS ca(i) 
			) u ON (u.UserAccountKey = f.UserAccountKey AND u.TenantID = f.TenantID)

		INNER JOIN Report.tbShopPointTranTypeLocale AS ptt
			ON f.PointTranType = ptt.PointTranType AND ptt.LocaleCode = ''en-us''
		LEFT OUTER JOIN Report.ExecutedAwardFact AS a
			ON f.UserRecognitionID = a.UserRecognitionID and f.TenantID = a.TenantID
		LEFT OUTER JOIN Report.RecognitionTypeLocale AS r
			ON a.RecognitionTypeID = r.RecognitionTypeID AND a.TenantID = r.TenantID AND r.LocaleCode = ''en-us''
		LEFT OUTER JOIN Report.ProgramLocale AS p
			ON a.ProgramID = p.ProgramID AND a.TenantID = p.TenantID AND p.LocaleCode = ''en-us''
		LEFT JOIN Report.GetUserOrgTreeWithAncestor(' + Cast(@UserID as varchar) + ',' + Cast(@TenantID as varchar) + ')
			ON GetUserOrgTreeWithAncestor.UserAccountID = u.UserAccountID
	WHERE t.TenantID = ''' + Cast(@TenantID as varchar) +  ''' ' + @WHERE + ' 
) a'


	Insert INTO [Report].[RDLQueryHistory] ([Timestamp], SQLQuery)
	Select GETDATE(), @Query

    Exec (@QUERY)

	--select @QUERY
	--select @WHERE

END

