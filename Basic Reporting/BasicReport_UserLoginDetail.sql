USE [MaestroReporting]
GO

/****** Object:  StoredProcedure [Report].[BasicReport_UserLoginDetail]    Script Date: 3/9/2015 2:27:22 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [Report].[BasicReport_UserLoginDetail]
@TenantID int,
@UserID int,
@StartLoginDateTime varchar(1000),
@EndLoginDateTime varchar(1000),
@ShipCountryName varchar(1000),
@UserAccessLevelCode varchar(1000),
@UserEmploymentStatusCode varchar(1000),
@PublicGroupName varchar(1000)
AS
BEGIN

Declare @Query varchar(max)
Declare @Where varchar(max)

Set @Where = ''

if((ltrim(rtrim(@StartLoginDateTime))  <> '' and ltrim(rtrim(@EndLoginDateTime)) <> '') and (@StartLoginDateTime  is not null and @EndLoginDateTime is not null))
	SELECT @WHERE = @WHERE + ' AND ulgfct.LoginDT between ''' + @StartLoginDateTime + ''' and ''' + @EndLoginDateTime + ''''

if (ltrim(rtrim(@ShipCountryName)) <> '' and @ShipCountryName is not null)
	SELECT @WHERE = @WHERE + ' AND usract.HistShipCountryCode = ''' + @ShipCountryName + ''''

if (ltrim(rtrim(@UserAccessLevelCode)) <> '' and @UserAccessLevelCode is not null)
	SELECT @WHERE = @WHERE + ' AND usract.HistUserAccessLevelCode = ''' + @UserAccessLevelCode + ''''

if (ltrim(rtrim(@UserEmploymentStatusCode)) <> '' and @UserEmploymentStatusCode is not null)
	SELECT @WHERE = @WHERE + ' AND usract.HistUserEmploymentStatus = ''' + @UserEmploymentStatusCode + ''''

if (ltrim(rtrim(@PublicGroupName)) <> '' and @PublicGroupName is not null and @PublicGroupName <> 'no_value_passed')
	SELECT @WHERE = @WHERE + ' AND usract.HistPublicGroupNameList like ''%' + @PublicGroupName + '%'''


SET @Query = 
	  ' SELECT usract.UserAccountID
		,usract.UserCustAcctCode
		,usract.UserLastName
		,usract.UserFirstName
		,usract.UserFullName
		,usract.UserEmailAddr
		,usract.HistShipCountryCode
		,usract.HistShipCountryName
		,usract.HistUserAccessLevelCode
		,usract.HistUserEmploymentStatus
		,usract.HistPublicGroupNameList
		,usract.AdminRoleNameList
		,ulgfct.LoginDT AS LoginDateTime
		,Custom.CustomAttributeName
		,Custom.CustomAttributeValue
	--,1 AS Lvl � GetUserOrgTreeWithAncestor.Lvl
	FROM Report.Tenant t
	INNER JOIN Report.UserLoginFact AS ulgfct ON t.TenantID = ulgfct.TenantID
	INNER JOIN (
		SELECT TenantID
			,UserAccountKey
			,UserAccountID
			,[InternalUserCode] AS UserCustAcctCode
			,PersonLastName AS UserLastName
			,PersonFirstName AS UserFirstName
			,[PersonFullName] AS UserFullName
			,[EmailLogin] AS UserEmailAddr
			,ShipCountryCode AS HistShipCountryCode
			,ShipCountryName AS HistShipCountryName
			,UserAccessLevelCode AS HistUserAccessLevelCode
			,UserEmploymentStatus AS HistUserEmploymentStatus
			,Report.GetPublicGroupNameList(''' + Cast(@TenantID as varchar(10)) + ''', PublicGroupIDList) AS HistPublicGroupNameList
			,[AdminRoleNameList] AS AdminRoleNameList
		FROM [Report].[UserAccount]
		) usract ON usract.UserAccountKey = ulgfct.UserAccountKey
		AND usract.TenantID = ulgfct.TenantID
	LEFT JOIN Report.UserCustomValue Custom ON usract.TenantID = t.TenantID AND usract.UserAccountKey = Custom.UserAccountKey
	WHERE t.TenantID = ''' + Cast(@TenantID as varchar(10)) + ''' ' + @WHERE


	Insert INTO [Report].[RDLQueryHistory] ([Timestamp], SQLQuery)
	Select GETDATE(), @Query

    Exec (@QUERY)

	--select @QUERY
	--select @WHERE



END

GO


