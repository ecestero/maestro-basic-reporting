USE [MaestroReporting]
GO

/****** Object:  StoredProcedure [Report].[BasicReport_ExecutedAwardDetail]    Script Date: 3/9/2015 2:22:03 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



ALTER PROCEDURE [Report].[BasicReport_ExecutedAwardDetail]
@TenantID int,
@UserID int,
@StartAwardCreationDateTime varchar(1000),
@EndAwardCreationDateTime varchar(1000),
@ProgramName varchar(1000),
@IsFeaturedProgram varchar(1000),
@RecognitionTypeName varchar(1000),
@CurrentAwardCriteriaName varchar(1000),
@StartAwardExecutionDateTime varchar(1000),
@EndAwardExecutionDateTime varchar(1000),
@RecipientShipCountry varchar(1000),
@RecipientEmploymentStatus varchar(1000),
@RecipientAccessLevel varchar(1000),
@InitiatorEmploymentStatus varchar(1000),
@InitiatorAccessLevel varchar(1000),
@InitiatorShipCountry varchar(1000)
AS
BEGIN


DECLARE @QUERY varchar(max)
DECLARE @WHERE varchar(max)

SET @WHERE = ''

if((@StartAwardCreationDateTime  <> '' and @EndAwardCreationDateTime <> '') and (@StartAwardCreationDateTime  is not null and @EndAwardCreationDateTime is not null))
	SELECT @WHERE = @WHERE + ' and [awdfct].[AwardCreatedDT] between ''' + CONVERT(varchar(30), @StartAwardCreationDateTime,121) +  ''' and ''' + CONVERT(VARCHAR, @EndAwardCreationDateTime ,121) + ''' '

if (@ProgramName <> '' and @ProgramName is not null and @ProgramName <> 'no_value_passed')
	SELECT @WHERE = @WHERE + ' AND [pgmloc].ProgramName = ''' + @ProgramName + ''''

--if(@IsFeaturedProgram  <> '' and @IsFeaturedProgram is not null)
--	SELECT @WHERE = @WHERE + ' AND [awdfct].IsFeaturedProgramID = ''' + @IsFeaturedProgram  + ''''

if(@RecognitionTypeName  <> '' and @RecognitionTypeName is not null and @RecognitionTypeName <> 'no_value_passed')
	SELECT @WHERE = @WHERE + ' AND [rctloc].RecognitionTypeName = ''' + @RecognitionTypeName  + ''''

if(@CurrentAwardCriteriaName  <> '' and @CurrentAwardCriteriaName is not null)
	SELECT @WHERE = @WHERE + ' AND [crtloc].CriteriaName = ''' + @CurrentAwardCriteriaName  + ''''

if((@StartAwardExecutionDateTime  <> '' and @EndAwardExecutionDateTime <> '') and (@StartAwardExecutionDateTime  is not null and @EndAwardExecutionDateTime is not null))
	SELECT @WHERE = @WHERE + ' AND [awdfct].[AwardExecutedDT] between ''' + @StartAwardExecutionDateTime + ''' and ''' + @EndAwardExecutionDateTime + ''''

if(@RecipientShipCountry  <> '' and @RecipientShipCountry is not null)
	SELECT @WHERE = @WHERE + ' AND [rcpact].RecipHistEffShipCountryCode = ''' + @RecipientShipCountry  + ''''

if(@RecipientEmploymentStatus  <> '' and @RecipientEmploymentStatus is not null)
	SELECT @WHERE = @WHERE + ' AND [rcpact].RecipEmpStatusCode = ''' + @RecipientEmploymentStatus  + ''''

if(@RecipientAccessLevel  <> '' and @RecipientAccessLevel is not null)
	SELECT @WHERE = @WHERE + ' AND [rcpact].RecipAccessLevelCode = ''' + @RecipientAccessLevel  + ''''

if(@InitiatorEmploymentStatus  <> '' and @InitiatorEmploymentStatus is not null)
	SELECT @WHERE = @WHERE + ' AND [gvract].InitiatorEmpStatusCode = ''' + @InitiatorEmploymentStatus  + ''''

if(@InitiatorAccessLevel  <> '' and @InitiatorAccessLevel is not null)
	SELECT @WHERE = @WHERE + ' AND [gvract].InitiatorAccessLevelCode = ''' + @InitiatorAccessLevel  + ''''

if(@InitiatorShipCountry  <> '' and @InitiatorShipCountry is not null)
	SELECT @WHERE = @WHERE + ' AND [gvract].InitiatorHistEffShipCountryCode = ''' + @InitiatorShipCountry  + ''''

--SELECT @WHERE

SET @QUERY = '	SELECT AwardID, AwdSourceType, CurrActivityTypeName, ProgramCode, CurrProgramName, ProgramSelector,
						CurrIsFeaturedProgram, RecogTypeCode, CurrRecogTypeName, ApprovalWasRequired, AwdPointAmt,
						AwdPointSelector, ConsumedBudAcctID, BudgetWasRequired, BudAcctType, BudGroupID, CurrBudGroupName,
						HistPrivBudgetAcctOwnerCurrFullName, CurrAwdCriteriaName, AwdCriteriaSelector, CurrAwdCriteriaSetName,
						CurrECardTemplateName, AwdCreatedDateTime, AwdCreatedFiscYear, AwdCreatedFiscTrim, AwdCreatedFiscQtr, AwdCreatedCalMth,
						AwdExecutedDateTime, AwdExecutedFiscYear, AwdExecutedFiscTrim, AwdExecutedFiscQtr, AwdExecutedCalMth,
						GiverNote, RecipCustAcctCode, RecipFullName, RecipEmailAddr, RecipHistEffShipCountryCode,
						RecipEmpStatusCode, RecipEmpStatusDescr, RecipAccessLevelCode, RecipAccessLevelDescr,
						InitiatorCustAcctCode, InitiatorFullName, InitiatorEmailAddr, InitiatorHistEffShipCountryCode,
						InitiatorEmpStatusCode, InitiatorEmpStatusDescr, InitiatorAccessLevelCode, InitiatorAccessLevelDescr,
						RecipCustomAttributeName, RecipCustomAttributeValue, InitiatorCustomAttributeName, InitiatorCustomAttributeValue, CustomAttributeID
						FROM
						(SELECT
						[awdfct].UserRecognitionID AS AwardID
						,[awdfct].AwardSourceType AS AwdSourceType
						,[actloc].ActivityTypeName AS CurrActivityTypeName
						,[pgmloc].ProgramCode AS ProgramCode
						,[pgmloc].ProgramName AS CurrProgramName
						,[awdfct].ProgramSelector
						,ISNULL([awdfct].IsFeaturedProgramID, 0) AS CurrIsFeaturedProgram
						,[rctloc].RecognitionTypeCode AS RecogTypeCode
						,[rctloc].RecognitionTypeName AS CurrRecogTypeName
						,ISNULL([awdfct].ApprovalWasRequired, 0) AS ApprovalWasRequired
						,[awdfct].AwardAmount AS AwdPointAmt
						,[awdfct].AwardPointSelector AS AwdPointSelector
						,[awdfct].IsMonetary
						,[awdfct].ConsumedBudgetAccountID AS ConsumedBudAcctID
						,[awdfct].BudgetWasRequired
						,[awdfct].BudgetGroupType AS BudAcctType
						,[awdfct].BudgetGroupID AS BudGroupID
						,[budloc].BudgetGroupName AS CurrBudGroupName
						,[budact].PersonFullName AS HistPrivBudgetAcctOwnerCurrFullName
						,[crtloc].CriteriaName AS CurrAwdCriteriaName
						,[awdfct].AwardCriteriaSelector AS AwdCriteriaSelector
						,[crsloc].CriteriaSetName AS CurrAwdCriteriaSetName
						,[ectloc].TemplateName AS CurrECardTemplateName
						,[awdfct].[AwardCreatedDT] AS AwdCreatedDateTime
						,[awdfct].[AwardCreatedFiscalYear] AS AwdCreatedFiscYear
						,[awdfct].[AwardCreatedFiscalTrimester] AS AwdCreatedFiscTrim
						,[awdfct].[AwardCreatedFiscalQuarter] AS AwdCreatedFiscQtr
						,[awdfct].[AwardCreatedCalendarMonth] AS AwdCreatedCalMth
						,[awdfct].[AwardExecutedDT] AS AwdExecutedDateTime
						,[awdfct].[AwardExecutedFiscalYear] AS AwdExecutedFiscYear
						,[awdfct].[AwardExecutedFiscalTrimester] AS AwdExecutedFiscTrim
						,[awdfct].[AwardExecutedFiscalQuarter] AS AwdExecutedFiscQtr
						,[awdfct].[AwardExecutedCalendarMonth] AS AwdExecutedCalMth
						,[awdfct].[GiverMessage] AS GiverNote
						,[rcpact].RecipCustAcctCode
						,[rcpact].RecipFullName
						,[rcpact].RecipEmailAddr
						,[rcpact].RecipHistEffShipCountryCode
						,[rcpact].RecipEmpStatusCode
						,[rcpact].RecipEmpStatusDescr
						,[rcpact].RecipAccessLevelCode
						,[rcpact].RecipAccessLevelDescr
						,[gvract].InitiatorCustAcctCode
						,[gvract].InitiatorFullName
						,[gvract].InitiatorEmailAddr
						,[gvract].InitiatorHistEffShipCountryCode
						,[gvract].InitiatorEmpStatusCode
						,[gvract].InitiatorEmpStatusDescr
						,[gvract].InitiatorAccessLevelCode
						,[gvract].InitiatorAccessLevelDescr
						,[rcpact].ReecipCustomAttributeID AS CustomAttributeID
						,[rcpact].RecipCustomAttributeName
						,[rcpact].RecipCustomAttributeValue
						,[gvract].InitiatorCustomAttributeName
						,[gvract].InitiatorCustomAttributeValue
						--,1 as Lvl --[GetUserOrgTreeWithAncestor].Lvl
					FROM Report.Tenant t
						INNER JOIN [Report].[ExecutedAwardFact] AS [awdfct] ON awdfct.TenantID = t.TenantID
						INNER JOIN [Report].[RecognitionTypeLocale] AS [rctloc]
							ON ([awdfct].RecognitionTypeID = rctloc.RecognitionTypeID AND [rctloc].TenantID = t.TenantID)
					 LEFT OUTER JOIN (
							SELECT ua.[TenantID] as RecipTenantID
								,ua.[UserAccountKey] as RecipUserAccountKey
								,ua.[UserAccountID] as RecipUserAccountID
								,ua.[InternalUserCode] AS RecipCustAcctCode
								,ua.[PersonFullName] AS RecipFullName
								,ua.[EmailLogin] AS RecipEmailAddr
								,ua.[ShipCountryCode] AS RecipHistEffShipCountryCode
								,ua.[UserEmploymentStatus] AS RecipEmpStatusCode
								,es.[EmploymentStatusDesc] AS RecipEmpStatusDescr
								,ua.[UserAccessLevelCode] AS RecipAccessLevelCode
								,al.[AccessLevelDesc] AS RecipAccessLevelDescr
								,ucv.[CustomAttributeID] as ReecipCustomAttributeID
								,ucv.[CustomAttributeName] AS RecipCustomAttributeName
								,ucv.[CustomAttributeValue] AS RecipCustomAttributeValue
								FROM [Report].[UserAccount] ua
								LEFT JOIN [Report].[AccessLevelLkp] al ON (ua.[UserAccessLevelCode] = al.[AccessLevelCode] AND al.[TenantID] = ua.TenantID)
								LEFT JOIN [Report].[EmploymentStatusLkp] es ON (ua.[UserEmploymentStatus] = es.[EmploymentStatusCode] AND es.[TenantID] = ua.TenantID)
								LEFT OUTER JOIN Report.UserCustomValue ucv ON ua.TenantID = ucv.TenantID AND ua.UserAccountKey = ucv.UserAccountKey
							--SELECT ua.[TenantID] as RecipTenantID
							--	,ua.[UserAccountKey] as RecipUserAccountKey
							--	,ua.[UserAccountID] as RecipUserAccountID
							--	,ua.[InternalUserCode] AS RecipCustAcctCode
							--	,ua.[PersonFullName] AS RecipFullName
							--	,ua.[EmailLogin] AS RecipEmailAddr
							--	,ua.[ShipCountryCode] AS RecipHistEffShipCountryCode
							--	,ua.[UserEmploymentStatus] AS RecipEmpStatusCode
							--	,es.[EmploymentStatusDesc] AS RecipEmpStatusDescr
							--	,ua.[UserAccessLevelCode] AS RecipAccessLevelCode
							--	,al.[AccessLevelDesc] AS RecipAccessLevelDescr ,ca.i.value(''(@name)'',''nvarchar(50)'') as RecipCustomAttributeName
							--	,ca.i.value(''(@value)'',''nvarchar(150)'') as RecipCustomAttributeValue
							--	FROM [Report].[UserAccount] ua
							--	LEFT JOIN [Report].[AccessLevelLkp] al ON (ua.[UserAccessLevelCode] = al.[AccessLevelCode] AND al.[TenantID] = @TenantID)
							--	LEFT JOIN [Report].[EmploymentStatusLkp] es ON (ua.[UserEmploymentStatus] = es.[EmploymentStatusCode] AND es.[TenantID] = @TenantID)
							--	CROSS APPLY CustomAttributes.nodes(''/ucv'') AS ca(i)
								) [rcpact] ON ([awdfct].RecipientUserAccountKey = [rcpact].RecipUserAccountKey) AND ([rcpact].RecipTenantID = [awdfct].TenantID)
							LEFT OUTER JOIN (
							SELECT ua.[TenantID] AS InitiatorTenantID
								,ua.[UserAccountKey] AS InitiatorUserAccountKey
								,ua.[UserAccountID] AS InitiatorUserAccountID
								,ua.[InternalUserCode] AS InitiatorCustAcctCode
								,ua.[PersonFullName] AS InitiatorFullName
								,ua.[EmailLogin] AS InitiatorEmailAddr
								,ua.[ShipCountryCode] AS InitiatorHistEffShipCountryCode
								,ua.[UserEmploymentStatus] AS InitiatorEmpStatusCode
								,es.[EmploymentStatusDesc] AS InitiatorEmpStatusDescr
								,ua.[UserAccessLevelCode] AS InitiatorAccessLevelCode
								,al.[AccessLevelDesc] AS InitiatorAccessLevelDescr
								,ucv.[CustomAttributeID] as InitiatorCustomAttributeID
								,ucv.[CustomAttributeName] as InitiatorCustomAttributeName
								,ucv.[CustomAttributeValue] as InitiatorCustomAttributeValue
								FROM [Report].[UserAccount] ua
								LEFT JOIN [Report].[AccessLevelLkp] al ON (ua.[UserAccessLevelCode] = al.[AccessLevelCode] AND al.[TenantID] = ua.[TenantID])
								LEFT JOIN [Report].[EmploymentStatusLkp] es ON (ua.[UserEmploymentStatus] = es.[EmploymentStatusCode] AND es.[TenantID] = ua.[TenantID])
								LEFT OUTER JOIN Report.UserCustomValue ucv ON ua.TenantID = ucv.TenantID AND ua.UserAccountKey = ucv.UserAccountKey
							--SELECT ua.[TenantID] AS InitiatorTenantID
							--	,ua.[UserAccountKey] AS InitiatorUserAccountKey
							--	,ua.[UserAccountID] AS InitiatorUserAccountID
							--	,ua.[InternalUserCode] AS InitiatorCustAcctCode
							--	,ua.[PersonFullName] AS InitiatorFullName
							--	,ua.[EmailLogin] AS InitiatorEmailAddr
							--	,ua.[ShipCountryCode] AS InitiatorHistEffShipCountryCode
							--	,ua.[UserEmploymentStatus] AS InitiatorEmpStatusCode
							--	,es.[EmploymentStatusDesc] AS InitiatorEmpStatusDescr
							--	,ua.[UserAccessLevelCode] AS InitiatorAccessLevelCode
							--	,al.[AccessLevelDesc] AS InitiatorAccessLevelDescr ,ca.i.value(''(@name)'',''nvarchar(50)'') as InitiatorCustomAttributeName
							--	,ca.i.value(''(@value)'',''nvarchar(150)'') as InitiatorCustomAttributeValue
							--	FROM [Report].[UserAccount] ua
							--	LEFT JOIN [Report].[AccessLevelLkp] al ON (ua.[UserAccessLevelCode] = al.[AccessLevelCode] AND al.[TenantID] = ua.[TenantID])
							--	LEFT JOIN [Report].[EmploymentStatusLkp] es ON (ua.[UserEmploymentStatus] = es.[EmploymentStatusCode] AND es.[TenantID] = ua.[TenantID])
							--	CROSS APPLY CustomAttributes.nodes(''/ucv'') AS ca(i)
								) [gvract] ON ([awdfct].InitiatorUserAccountKey = [gvract].InitiatorUserAccountKey) AND ([gvract].InitiatorTenantID = [awdfct].TenantID)
										AND ([gvract].InitiatorCustomAttributeID = [rcpact].ReecipCustomAttributeID)
						LEFT OUTER JOIN [Report].[ActivityTypeLocale] AS [actloc]
							ON ([awdfct].ActivityTypeID = [actloc].ActivityTypeID AND [actloc].TenantID = t.TenantID)
						LEFT OUTER JOIN [Report].[ProgramLocale] AS [pgmloc]
							ON ([awdfct].ProgramID = [pgmloc].ProgramID AND [pgmloc].TenantID = t.TenantID)
						LEFT OUTER JOIN [Report].[BudgetGroupLocale] AS [budloc]
							ON ([awdfct].BudgetGroupID = [budloc].BudgetGroupID AND [budloc].TenantID = t.TenantID)
						LEFT OUTER JOIN [Report].[BudgetAccountFact] AS bdafct
							ON ([awdfct].ConsumedBudgetAccountID = bdafct.BudgetAccountID AND bdafct.TenantID = t.TenantID
								AND awdfct.AwardCreatedDT BETWEEN bdafct.RowEffectiveDT AND ISNULL(bdafct.RowExpirationDT, DATEADD(DD,1,GETUTCDATE())) )
						LEFT OUTER JOIN [Report].[UserAccount] AS [budact]
							ON (bdafct.OwnerUserAccountKey = [budact].UserAccountKey AND [budact].TenantID = t.TenantID)
						LEFT OUTER JOIN [Report].[CriteriaLocale] AS [crtloc]
							ON ([awdfct].CriteriaID = [crtloc].CriteriaID AND [crtloc].TenantID = t.TenantID)
						LEFT OUTER JOIN [Report].[CriteriaSetLocale] AS [crsloc]
							ON ([awdfct].CriteriaSetID = [crsloc].CriteriaSetID AND [crsloc].TenantID = t.TenantID)
						LEFT OUTER JOIN [Report].[ECardTemplateLocale] AS [ectloc]
							ON ([awdfct].ECardTemplateID = [ectloc].ECardTemplateID)
						--LEFT OUTER JOIN [Report].[GetUserOrgTreeWithAncestor](@UserID, @TenantID) ON [GetUserOrgTreeWithAncestor].[UserAccountID] = [gvract].[InitiatorUserAccountID]
						 WHERE t.TenantID = ' + Cast(@TenantId as varchar) +
						 + @WHERE + '
					) a
					ORDER BY 1'

	Insert INTO [Report].[RDLQueryHistory] ([Timestamp], SQLQuery)
	Select GETDATE(), @Query

	Exec (@QUERY)

	--select @QUERY
	--select @WHERE

END



GO


