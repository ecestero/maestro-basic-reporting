USE [MaestroReporting]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [Report].[BasicReport_UserProfileDetail]
@TenantID int,
@UserID int,
@ShipCountry varchar(1000),
@UserAccessLevel varchar(1000),
@EmploymentStatus varchar(1000),
@PublicGroups varchar(1000),
@AdminRoles varchar(1000),
@IsManager varchar(1000),
@StartUserAddedDateTime varchar(1000),
@EndUserAddedDateTime varchar(1000),
@StartLastLoginDateTime varchar(1000),
@EndLastLoginDateTime varchar(1000),
@PointBalance varchar(1000),
@Programs varchar(1000)
AS
BEGIN

Declare @Query varchar(max)
Declare @Where varchar(max)

Set @Where = ''

if (ltrim(rtrim(@ShipCountry)) <> '' and @ShipCountry is not null)
	SELECT @WHERE = @WHERE + ' AND usract.ShipCountryCode = ''' + @ShipCountry + ''''

if (ltrim(rtrim(@UserAccessLevel)) <> '' and @UserAccessLevel is not null)
	SELECT @WHERE = @WHERE + ' AND usract.UserAccessLevelCode = ''' + @UserAccessLevel + ''''

if (ltrim(rtrim(@EmploymentStatus)) <> '' and @EmploymentStatus is not null)
	SELECT @WHERE = @WHERE + ' AND usract.UserEmploymentStatus = ''' + @EmploymentStatus + ''''


if (ltrim(rtrim(@PublicGroups)) <> '' and @PublicGroups is not null and @PublicGroups <> 'no_value_passed')
	SELECT @WHERE = @WHERE + ' AND usract.PublicGroupNameList like ''%' + @PublicGroups + '%'''


if (ltrim(rtrim(@AdminRoles)) <> '' and @AdminRoles is not null and @AdminRoles <> 'no_value_passed')
	SELECT @WHERE = @WHERE + ' AND usract.AdminRoleNameList = ''' + @AdminRoles + ''''

if (ltrim(rtrim(@IsManager)) <> '' and @IsManager is not null)
	SELECT @WHERE = @WHERE + ' AND usract.UserIsManager = ''' + @IsManager + ''''

if((ltrim(rtrim(@StartUserAddedDateTime))  <> '' and ltrim(rtrim(@EndUserAddedDateTime)) <> '') and (@StartUserAddedDateTime  is not null and @EndUserAddedDateTime is not null))
	SELECT @WHERE = @WHERE + ' AND usract.UserCreateDateTime between ''' + @StartUserAddedDateTime + ''' and ''' + @EndUserAddedDateTime + ''''

if((ltrim(rtrim(@StartLastLoginDateTime))  <> '' and ltrim(rtrim(@EndLastLoginDateTime)) <> '') and (@StartLastLoginDateTime  is not null and @EndLastLoginDateTime is not null))
	SELECT @WHERE = @WHERE + ' AND usract.LastLoginDT between ''' + @StartLastLoginDateTime + ''' and ''' + @EndLastLoginDateTime + ''''

if (ltrim(rtrim(@PointBalance)) <> '' and @PointBalance is not null)
	SELECT @WHERE = @WHERE + ' AND usrfct.ShoppingPointBalance = ''' + @PointBalance + ''''

if (ltrim(rtrim(@Programs)) <> '' and @Programs is not null)
	SELECT @WHERE = @WHERE + ' AND usract.ProgramNameList = ''' + @Programs + ''''



SET @Query = '
	SELECT
		UserAccountID, CustAcctCode, UserLastName, UserFirstName, UserFullName, UserEmailAddr, ShipCountryCode
		, ManagerCustAcctCode, ManagerLastName, ManagerFirstName, ManagerFullName, ManagerEmailAddr
		, UserAccessLevelCode, UserEmploymentStatus, PublicGroupNameList, AdminRoleNameList
		, UserIsManager, UserCreateDateTime, LastLoginDT, FYTDLoginCount
		, PrivateBudgetAccountList, RecognitionTypeGiveList, RecognitionTypeReceiveList, ProgramNameList
		, FYTDShoppingPointsRec, FYTDShoppingPointsSpent, ShoppingPointBalance
		, UserCustomAttributeName, UserCustomAttributeValue
	FROM
		(
			SELECT
				usract.TenantID
				,usract.UserAccountKey
				,usract.UserAccountID
				,usract.InternalUserCode AS CustAcctCode
				,usract.PersonLastName AS UserLastName
				,usract.PersonFirstName AS UserFirstName
				,usract.PersonFullName AS UserFullName
				,usract.EmailLogin AS UserEmailAddr
				,usract.ShipCountryCode
				,usract.ShipCountryName
				,usract.ManagerAccountID AS ManagerUserID
				,usract.ManagerInternalUserCode AS ManagerCustAcctCode
				,usract.ManagerLastName
				,usract.ManagerFirstName
				,usract.ManagerFullName
				,usract.ManagerEmailAddress AS ManagerEmailAddr
				,usract.UserAccessLevelCode
				,usract.UserEmploymentStatus
				,Report.GetPublicGroupNameList(''' + Cast(@TenantID as varchar(10)) + ''',usract.PublicGroupIDList) as PublicGroupNameList
				,usract.AdminRoleNameList
				,usract.UserIsManager
				,usract.RecCreatedDT AS UserCreateDateTime
				,usrlog.LastLoginDT
				,usrlog.FYTDLoginCount
				,Report.GetPrivateBudgetGroupList(''' + Cast(@TenantID as varchar(10)) + ''',usract.PrivateBudgetGroupIDList,NULL) as PrivateBudgetAccountList
				,Report.GetRecognitionTypeNameList(''' + Cast(@TenantID as varchar(10)) + ''',usract.RecognitionTypeIDGiveList,NULL) as RecognitionTypeGiveList
				,Report.GetRecognitionTypeNameList(''' + Cast(@TenantID as varchar(10)) + ''',usract.RecognitionTypeIDReceiveList,NULL) as RecognitionTypeReceiveList
				,Report.GetProgramNameList(''' + Cast(@TenantID as varchar(10)) + ''',usract.ProgramIDList,NULL) as ProgramNameList
				,usrfct.FYTDShoppingPointRecieved AS FYTDShoppingPointsRec
				,usrfct.FYTDShoppingPointSpent AS FYTDShoppingPointsSpent
				,usrfct.ShoppingPointBalance
				,usract.RecCreatedFiscalYear AS UserCreateFiscYear
				,usract.RecCreatedFiscalQuarter AS UserCreateFiscQtr
				,usract.RecCreatedFiscalTrimester AS UserCreateFiscTrim
				,usract.RecCreatedCalMonth AS UserCreateCalMth
				,ucv.CustomAttributeName as UserCustomAttributeName
				,ucv.CustomAttributeValue as UserCustomAttributeValue
				,ucv.CustomAttributeID
				,ucv.FileColumnPosition
			FROM Report.Tenant t
				INNER JOIN Report.vwUserAccountCurrent AS usract ON (t.TenantID = usract.TenantID)
				LEFT JOIN Report.vwUserAccountFactCurrent AS usrfct ON (usract.UserAccountKey = usrfct.UserAccountKey)
				LEFT OUTER JOIN
				(
					SELECT ulf.TenantID, ua.UserAccountID
						,MAX(LoginDT) as LastLoginDT
						,COUNT(DISTINCT LoginDT) as FYTDLoginCount
					FROM Report.UserLoginFact ulf
						JOIN Report.UserAccount ua ON ulf.TenantID = ua.TenantID AND ulf.UserAccountKey = ua.UserAccountKey
					WHERE LoginFiscalYear =
							(
								SELECT FiscalYear
								FROM [Report].[GetCurrentFiscalPeriod] (''' + Cast(@TenantID as varchar(10)) + ''')
								WHERE GetCurrentFiscalPeriod.CalendarDT = CONVERT(date,GETUTCDATE())
							)
					GROUP BY ulf.TenantID, ua.UserAccountID
				) usrlog
					ON usrlog.TenantID = usract.TenantID AND usrlog.UserAccountID = usract.UserAccountID
				LEFT JOIN Report.UserCustomValue ucv ON usract.TenantID = ucv.TenantID AND usract.UserAccountKey = ucv.UserAccountKey
		WHERE t.TenantID = ''' + Cast(@TenantID as varchar(10)) + ''' '
			+ @WHERE + '
		) a

		ORDER BY 1'

	INSERT INTO [Report].[RDLQueryHistory] ([Timestamp], SQLQuery)
	Select GETDATE(), @Query

	Exec (@QUERY)

	--select @QUERY
	--select @WHERE

END
